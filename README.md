# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

UICollectionView is highly customizable class for presenting your content in almost any layout you want. 
It implements in a similar way to UITableView — using UICollectionViewDataSource and UICollectionViewDelegate protocols. 
Let’s take a look at how to do it using Swift.


### How do I get set up? ###

I want to do everything in code, so let’s delete the storyboard that was provided to us by Xcode.
Open up 
Project Navigator
 (⌘1).
Select your project here, then in menu at the right of this project button under the 
Targets
 section select your project name. It’s 
Gridtest
 in my case.
Clear 
Main Interface
 field.
Delete 
Main.storyboard
 from your project.
Alright. There’s no storyboards now, but because of this now we need to initialize a window programmatically, just like in Objective-C. 
Select AppDelegate.swift in Project Navigator and update application(_:didFinishLaunchingWithOptions:) method with this code:


func application(application: UIApplication, didFinishLaunchingWithOptions launchOptions: [NSObject : AnyObject]?) -> Bool 
{

	window = UIWindow(frame: UIScreen.mainScreen().bounds)

	window!.backgroundColor = UIColor.redColor()

	window!.rootViewController = ViewController()

	window!.makeKeyAndVisible()

	return true
}

This little piece of code (as the name tells us) gets called right after application finish its initialization process. 
Here we initialize our windowproperty, set it’s background color to be red, set an instance of ViewController class to be window’s 
root view controller and show it to user. Window is where everything is displayed in an iOS application.
Run the app and you will see that the screen is filled with red. Good. Window is here, so we can move on. An instance of 
ViewController class is a root view controller of our window, but it’s transparent. Let’s fill it with UICollectionView!

### Contribution guidelines ###

You can contribute in this projet to make it even more simpler for beginers!

### Who do I talk to? ###
Admin:shaunakjagtap@gmail.com