//
//  ViewController.swift
//  CollectionViewDemo
//
//  Created by Rajesh on 29/11/16.
//  Copyright © 2016 CDAC. All rights reserved.
//

import UIKit

class ViewController: UIViewController , UICollectionViewDataSource, UICollectionViewDelegate
{

    @IBOutlet weak var myCollectionView: UICollectionView!
    
    var arrayImageDetails = Array<ImageDetail>();
    
    
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        self.view.backgroundColor = .red
        let details1=ImageDetail(file: "3.jpeg", title: "Amit")
        arrayImageDetails.append(details1);
        
        let details2=ImageDetail(file: "4.jpg", title: "Shaunak")
        arrayImageDetails.append(details2);
        
        let details3=ImageDetail(file: "5.jpeg", title: "Pratik")
        arrayImageDetails.append(details3);
        
        let details4=ImageDetail(file: "6.jpg", title: "Saly")
        arrayImageDetails.append(details4);
        
        let details5=ImageDetail(file: "7.jpg", title: "Paritosh")
        arrayImageDetails.append(details5);
        
        let details6=ImageDetail(file: "8.png", title: "Pooja")
        arrayImageDetails.append(details6);
        
        let details7=ImageDetail(file: "9.jpg", title: "Monika")
        arrayImageDetails.append(details7);
        
        let details8=ImageDetail(file: "10.png", title: "Tushar")
        arrayImageDetails.append(details8);
        
        let details9=ImageDetail(file: "burger.jpeg", title: "Nisha")
        arrayImageDetails.append(details9);
        
        let details10=ImageDetail(file: "dexter.jpg", title: "dexter")
        arrayImageDetails.append(details10);
        
        let details11=ImageDetail(file: "jonny.jpg", title: "Jonny")
        arrayImageDetails.append(details11);
        
        let details12=ImageDetail(file: "mojo.jpg", title: "Mojo")
        arrayImageDetails.append(details12);
        
        let details13=ImageDetail(file: "poppyee.jpg", title: "poppyee")
        arrayImageDetails.append(details13);
        
        let details14=ImageDetail(file: "scooby.jpg", title: "scooby")
        arrayImageDetails.append(details14);
        
        let details15=ImageDetail(file: "3.jpeg", title: "Amit")
        arrayImageDetails.append(details15);
        
        let details16=ImageDetail(file: "4.jpg", title: "Shaunak")
        arrayImageDetails.append(details16);
        
        let details17=ImageDetail(file: "5.jpeg", title: "Pratik")
        arrayImageDetails.append(details17);
        
        let details18=ImageDetail(file: "6.jpg", title: "Saly")
        arrayImageDetails.append(details18);
        
        let details19=ImageDetail(file: "7.jpg", title: "Paritosh")
        arrayImageDetails.append(details19);
        
        let details20=ImageDetail(file: "8.png", title: "Pooja")
        arrayImageDetails.append(details20);
        
        let details21=ImageDetail(file: "9.jpg", title: "Monika")
        arrayImageDetails.append(details21);
        
        let details22=ImageDetail(file: "10.png", title: "Tushar")
        arrayImageDetails.append(details22);
        
        let details23=ImageDetail(file: "burger.jpeg", title: "Nisha")
        arrayImageDetails.append(details23);
        
        let details24=ImageDetail(file: "dexter.jpg", title: "dexter")
        arrayImageDetails.append(details24);

        let cellNib=UINib(nibName: "MyCollectionCell", bundle: nil);
        myCollectionView.register(cellNib,forCellWithReuseIdentifier: "mycell")
        
        myCollectionView.contentMode = .scaleAspectFill
        myCollectionView.frame = self.view.frame
        
        myCollectionView.dataSource = self;
        myCollectionView.delegate = self;
        
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    func setGradientBackground() {
        let colorTop =  UIColor(red: 255.0/255.0, green: 149.0/255.0, blue: 0.0/255.0, alpha: 1.0).cgColor
        let colorBottom = UIColor(red: 255.0/255.0, green: 94.0/255.0, blue: 58.0/255.0, alpha: 1.0).cgColor
        
        let gradientLayer = CAGradientLayer()
        gradientLayer.colors = [ colorTop, colorBottom]
        gradientLayer.locations = [ 0.0, 1.0]
        gradientLayer.frame = self.view.bounds
        
        self.view.layer.addSublayer(gradientLayer)
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        return arrayImageDetails.count;
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        let imageDetail = arrayImageDetails[indexPath.row]
        
        let cell = myCollectionView.dequeueReusableCell(withReuseIdentifier: "mycell", for: indexPath) as! MyCollectionCell
        
        cell.myImageView.image = UIImage(named:imageDetail.imageFile!);
        cell.imageTitleLabel.text = imageDetail.imageTitle;
        cell.imageDetail = imageDetail;
        cell.controller = self;
        
        
        return cell;
    }
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

