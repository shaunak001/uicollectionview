//
//  imageDetail.swift
//  CollectionViewDemo
//
//  Created by Rajesh on 29/11/16.
//  Copyright © 2016 CDAC. All rights reserved.
//

import Foundation
class ImageDetail
{
    var imageFile:String?;
    var imageTitle:String?;
    
    init(){}
    
    init(file:String,title:String)
    {
        self.imageFile = file;
        self.imageTitle = title;
    }

    
}